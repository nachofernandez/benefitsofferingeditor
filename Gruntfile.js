module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-mocha-phantomjs');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-preprocess');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-cache-bust');
    grunt.loadNpmTasks('grunt-sync');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        meta: {
            banner: ''
        },
        cacheBust: {
            options: {
                baseDir: 'target/',
                encoding: 'utf8',
                algorithm: 'md5',
                length: 100,
                rename: false
            },
            assets: {
                files: [{
                    src: ['target/index.html']
                }]
            }
        },
        connect: {
            server: {
                options: {
                    livereload: false,
                    base: 'target/',
                    port: 9000
                }
            }
        },
        mocha_phantomjs: {
            all: ['test.html']
        },
        watch: {
            scripts: {
                files: [
                        'app/**/*.js',
                        'app/**/*.hbs',
                        'css/*.css',
                        '*.html'
                ],
                tasks: ['sync-local'],
                options: {
                    spawn: false,
                    livereload: true
                }
            }
        },
        sync: {
            local: {
                files: [
                    {src: ['css/**'], dest: 'target/'},
                    {src: ['app/**'], dest: 'target/'},
                    {src: ['test/**'], dest: 'target/'},
                    {src: ['images/**'], dest: 'target/'},
                    {src: ['vendor/**'], dest: 'target/'},
                    {src: ['*.html'], dest: 'target/'}
                ],
                pretend: false,
                verbose: true
            }
        },
        handlebars: {
            compile: {
                files: {
                    "target/app/templates.js": ["target/app/modules/**/*.hbs"]
                },
                options: {
                    namespace: 'Handlebars.templates',
                    processName: function(filePath) {
                        var pieces = filePath.split("/");
                        return pieces[pieces.length - 1].replace('.hbs', '');
                    }
                }
            }
        },
        concat: {
            app: {
                src: [
                    "target/app/templates.js",
                    "target/app/select2.js",
                    "target/app/app.js",
                    "target/app/entities/**/**.js",

                    "target/app/modules/**/**.js"
                ],
                dest: 'target/dist/eligibilityApp.dev.js'
            }
        },

        uglify: {
            min_all: {
                options: {
                    banner: '<%= meta.banner %>',
                    compress: {
                        drop_console: true
                    }
                },
                files: {
                    'target/dist/eligibilityApp.min.js': ['target/dist/eligibilityApp.dev.js']
                }
            }
        },
        cssmin: {
            options: {
                advanced: false,
                shorthandCompacting: false,
                roundingPrecision: -1,
                rebase: false,
                aggressiveMerging: false,
                keepSpecialComments: 0
            },
            concat: {
                files: {
                    'target/css/styles.css':
                    [   'target/css/styles.css' ]
                }
            },
            min_all: {
                files: [{
                    expand: true,
                    cwd: 'target/css',
                    src: ['styles.css'],
                    dest: 'target/css',
                    ext: '.min.css'
                }]
             }
        },
        clean: {
            target: ['target/'],
            directories: [
                            "target/test",
                            "target/app"],
            css: ["target/css/*.css", "!target/css/*.min.css"],
            test: ["target/test.html"]
        },
        copy: {
            create_structure: {
                files: [
                    {expand: true, src: ['css/**'], dest: 'target/'},
                    {expand: true, src: ['test/**'], dest: 'target/'},
                    {expand: true, src: ['images/**'], dest: 'target/'},
                    {expand: true, src: ['app/**'], dest: 'target/'},
                    {expand: true, src: ['*.html'], dest: 'target/'}
                ]
            }
        },

        preprocess : {
            options: {
                context : {VERSION: "1" }
            },
            build_local : {
                options : {
                    context : {
                        EXPLODED: true
                    }
                },
                files: [
                    {src: 'target/index.html', dest: 'target/index.html'}
                ]
            },
            build_prod : {
                options : {
                    context : {}
                },
                files: [
                    {src: 'target/index.html', dest: 'target/index.html'}
                ]
            },
            build_test : {
                options : {
                    context : {}
                },
                files: [
                    {src: 'target/index.html', dest: 'target/index.html'}
                ]
            }
        }
    });

    grunt.registerTask('test', ['connect:server', 'mocha_phantomjs']);


    grunt.registerTask("build-production", "Build Production App", function() {
        grunt.task.run("clean:target");
        grunt.task.run("copy:create_structure");
        grunt.task.run("preprocess:build_prod");
        grunt.task.run("handlebars");

        grunt.task.run("test");
        grunt.task.run("concat:app");

        grunt.task.run("uglify:min_all");

        grunt.task.run("cssmin:concat");
        grunt.task.run("cssmin:min_all");
        grunt.task.run("clean:css");
        grunt.task.run("clean:directories");

        grunt.task.run("clean:test");

        grunt.task.run("cacheBust");

    });

    grunt.registerTask("build-test", "Build Production App", function() {
        grunt.task.run("clean:target");
        grunt.task.run("copy:create_structure");
        grunt.task.run("preprocess:build_test");
        grunt.task.run("handlebars");

        grunt.task.run("concat:app");

        grunt.task.run("uglify:min_all");

        grunt.task.run("cssmin:concat");
        grunt.task.run("cssmin:min_all");
        grunt.task.run("clean:css");

        grunt.task.run("cacheBust");
    });

    grunt.registerTask("build-local", "Build App", function() {
        grunt.task.run("clean:target");
        grunt.task.run("copy:create_structure");
        grunt.task.run("preprocess:build_local");
        grunt.task.run("handlebars");
    });


    grunt.registerTask("sync-local", "Build webapp", function() {
        grunt.task.run("sync:local");
        grunt.task.run("preprocess:build_local");
        grunt.task.run("handlebars");
    });

    grunt.registerTask('serve', [
        'build-local',
        'connect:server',
        'watch'
    ]);

};
