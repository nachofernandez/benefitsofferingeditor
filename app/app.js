EligibilityApp = (function(Backbone, Marionette) {
    var App = new Marionette.Application();

    App.on("start", function(options) {
        App.plan_types = options.plan_types || {};
        App.service_url = options.service_url;
        this.addRegions({mainContent: options.containerSelector});

        App.Eligibility.Controller.show();
    });


    return App;
})(Backbone, Marionette);
