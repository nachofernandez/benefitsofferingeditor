EligibilityApp.module("Eligibility", function(Eligibility, App, Backbone, Marionette, $, _) {
    var PLANS_PLACEHOLDER = "+ Add a plan";

    Eligibility.ItemView = Marionette.ItemView.extend({
        template: Handlebars.templates.eligibility_item,
        tagName: "li",
        className: "row eligibility-item",
        ui: {
            nameEditor: "#nameEditor",
            handle: ".handle img"
        },
        collectionEvents: {
            "sync update":"toggleTexts"
        },
        events: {
            "drop":"updateOrder",
            "click #deleteBtn":"deleteItem"
        },
        initialize: function() {
            this.model.set("plan_types", App.plan_types);
        },
        onShow: function() {
            this.$el.effect("highlight"); //highlight yellow after add
            if (this.model.get("default")) {
                this.$el.addClass("default");
            }

            this.initNameEditor();
            this.initPlans();
            this.toggleTexts();
        },
        toggleTexts: function() {
              if (this.collection.length > 2) {
                  this.ui.handle.show();
              } else {
                  this.ui.handle.hide();
              }
        },
        updateOrder: function(event, newIndex) {
            var newOrder = newIndex + 1;
            Eligibility.Controller.synCollection(this.model, newOrder);
        },
        initPlans: function() {
            _.each(App.plan_types, function(value, key) {
                this.initPlanSelect(value);
            }, this);
        },
        initPlanSelect: function(planType) {
            var self = this;
            var $planElement = this.$el.find("select#"+planType);
            var $selectElement = $planElement.select2({
                placeholder: PLANS_PLACEHOLDER,
                data: Eligibility.Controller.getPlans().getByPlan(planType)
            });

            $selectElement.on("change", function(event) {
                self._toggleAddPlanText(event, planType, $planElement);
            });

            if (this.model.get(planType)) {
                $planElement.select2().val(this.model.get(planType)).trigger("change");
            }

            $selectElement.on("change", function(event, data){
                if (data) {  //highlight added element
                    var $element = $(event.target.nextSibling).find("li[title='"+data.text+"']");
                    $element.effect("highlight");
                }
                //Update and save model
                self.model.set(planType, $planElement.val());
                self.model.save();
            });
        },
        _toggleAddPlanText: function(event, planType, $planElement) {
            var plans = Eligibility.Controller.getPlans();
            var $addPlanElement = $(event.target.nextSibling).find("li.select2-search--inline");

            if ($planElement.val() &&
                $planElement.val().length === plans.getByPlan(planType).length) {
                //Hide add plan text
                $addPlanElement.hide();
            } else {
                $addPlanElement.show();
            }
        },
        initNameEditor: function() {
            var self = this;
            var options = {inputclass:"name-input", emptytext: "Name", mode: "inline",
                            type: "text", showbuttons: false, onblur: "submit",
                            unsavedclass: null};

            options.success = function(response, newValue) {
                self.model.set("name",newValue);
                self.model.save();
            };

            this.ui.nameEditor.editable(options);
        },
        deleteItem:function(e) {
            e.preventDefault();
            var self = this;
            this.$el.effect("highlight",{color: "#ff0000"}, function(){ //highlight red before delete
                Eligibility.Controller.remove(self.model);
            });
        }
    });

    Eligibility.MainView = Marionette.CompositeView.extend({
        template: Handlebars.templates.eligibility,
        childView: Eligibility.ItemView,
        childViewContainer: "#items_list",
        reorderOnSort: true,
        collectionEvents: {
            "sync update":"initSort"
        },
        events: {
            "click #addBtn":"addItem"
        },
        ui: {
            titles: "h5"
        },
        childViewOptions: function() {
            return {collection: this.collection}
        },
        addItem: function() {
            Eligibility.Controller.addNew();
        },
        onShow: function() {
            if (this.collection.length > 0) {
                this.initSort();
            }
        },
        initSort: function() {
            if (this.collection.length > 1) {
                this.ui.titles.show();
            } else {
                this.ui.titles.hide();
            }

            if (this.collection.length > 2) {
                this.$childViewContainer.sortable({
                    cancel: ".default",
                    handle: ".handle",
                    items: "li.eligibility-item:not(.default)",
                    appendTo: document.body
                });

                this.$childViewContainer.disableSelection();

                this.$childViewContainer.on("sortupdate", function(event,ui) {
                    ui.item.trigger('drop', ui.item.index());
                } );
            }
        }
    });
});