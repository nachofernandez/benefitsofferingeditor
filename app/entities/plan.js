EligibilityApp.module("Entities", function(Entities, App, Backbone, Marionette, $, _) {
    Entities.Plan = Backbone.Model.extend({
        idAttribute: "objectId",
        urlRoot: function() {
            return App.service_url + 'Plan/';
        },
        parse: function (response) {
            response.id = response.objectId;
            response.text = response.name;
            return response;
        }
    });

    Entities.PlanCollection = Backbone.Collection.extend({
        url: function() {
            return App.service_url + 'Plan/';
        },
        model: Entities.Plan,
        parse: function (response) {
            return response.results;
        },
        getByPlan: function (planType) {
            return _.map(this.where({type: planType}), function (model) {
                return model.toJSON();
            });
        }
    });
});