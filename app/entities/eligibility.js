EligibilityApp.module("Entities", function(Entities, App, Backbone, Marionette, $, _) {

    Entities.Eligibility = Backbone.Model.extend({
        defaults: {
            "default": false,
            "order": 1
        },
        idAttribute: "objectId",
        urlRoot: function() {
            return App.service_url + 'eligibility/';
        }
    });

    Entities.EligibilityCollection = Backbone.Collection.extend({
        url: function() {
            return App.service_url + 'eligibility/';
        },
        model: Entities.Eligibility,
        getDefault: function() {
            return this.findWhere({default:true});
        },
        comparator: function(a, b) {
            if (a.get("default")) { //Keep the default eligibility at the bottom
                return 1;
            } else if (b.get("default")) {
                return -1;
            } else if (a.get("order") > b.get("order")) {
                return 1;
            } else {
                return -1;
            }
        },
        deleteItem: function(modelToDelete) {
            var self = this;
            modelToDelete.destroy({success: function() {
                self.remove(modelToDelete);
            }});

            var order = 1;
            this.each(function(model){
                if (!model.get("default")) {
                    model.set("order", order++);
                    model.save();
                }
            });

            this.sort();
        },
        addNew: function(newModel) {
            //Reorder collection
            var order = 1;
            this.each(function(model) {
                if (!model.get("default")) {
                    model.set("order", ++order);
                    model.save();
                }
            });

            this.add(newModel);
            this.sort();
        },
        syncOrder: function(model, newOrder) {
            var modelWithNewOrder = this.findWhere({order: newOrder}); //Find the model to update the position
            modelWithNewOrder.set("order", model.get("order"));
            model.set("order", newOrder);

            this.sort();

            modelWithNewOrder.save();
            model.save();
        },
        parse: function(response) {
            return response.results;
        }
    });
});