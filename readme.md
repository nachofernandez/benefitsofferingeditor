
# Project Technology stack

## Main JS frameworks

* [jQuery](https://jquery.com/)
* [Backbone](http://backbonejs.org/)
* [Marionette](http://marionettejs.com/)
* [Handlebars](http://handlebarsjs.com/)

## Testing frameworks

* [Mocha](https://mochajs.org/)
* [Chai](http://chaijs.com/)
* [Sinon](http://sinonjs.org/)

## Build task runner

* [Grunt](http://gruntjs.com/)

## Backend

This project is using [Parse](https://www.parse.com/) as backend service, to store the data and deploy the application.

# Project Setup

## Dependencies

You should have npm installed in your system.

## Download and install modules

npm install

##Run dev server

grunt serve
After run this command the application should be running on: http://localhost:9000

## Run tests

grunt test

##Compile app

grunt build --env=prod

This command will run all the test and generate the minimized files. 
