var expect = chai.expect;
describe('Eligibility Model', function() {
    describe('Create new eligibility model', function() {
        var testEligibility = new EligibilityApp.Entities.Eligibility();

        it('Model not null', function() {
            expect(testEligibility).to.exist;
        });

        it('Default must be false', function() {
            expect(testEligibility.get("default")).to.equal(false);
        });

        it('Default order must be 1', function() {
            expect(testEligibility.get("order")).to.equal(1);
        });
    });
});

describe('Eligibility Collection', function() {
    describe('Create new eligibility collection', function() {
        var eligibilityCollection = new EligibilityApp.Entities.EligibilityCollection();

        it('Collection not null', function() {
            expect(eligibilityCollection).to.exist;
        });

        it('Collection must be empty null', function() {
            expect(eligibilityCollection.length).to.equal(0);
        });

        var server;

        beforeEach(function() {
            server = sinon.fakeServer.create();
        });

        afterEach(function () {
            server.restore();
        });

        it("Fetch Collection", function () {
            server.respondWith("GET", "/eligibility/",
                [200, { "Content-Type": "application/json" },
                    JSON.stringify({
                        "results": [
                            {
                                "createdAt": "2015-12-12T17:29:03.106Z",
                                "default": true,
                                "objectId": "ZYhVxiDASX"
                            }
                        ]
                    })
                ]);

            eligibilityCollection.fetch();

            server.respond(); // Process all requests so far

            expect(eligibilityCollection.length).to.equal(1);
        });
    });

    describe('Add/Delete/Sort models', function() {
        var eligibilityCollection = new EligibilityApp.Entities.EligibilityCollection();

        it('Add default eligibility', function() {
            var defaultEligibility = new EligibilityApp.Entities.Eligibility({default: true});
            eligibilityCollection.add(defaultEligibility);
            expect(eligibilityCollection.length).to.equal(1);
        });

        it('Add new eligibility', function() {
            var newEligibility = new EligibilityApp.Entities.Eligibility({objectId: "0"});
            eligibilityCollection.add(newEligibility);
            expect(eligibilityCollection.length).to.equal(2);
        });

        it('Default must be the last eligibility', function() {
            var lastItem = eligibilityCollection.last();
            expect(lastItem.get("default")).to.equal(true);
        });

        it('Get default eligibility', function() {
            var defEli = eligibilityCollection.getDefault();
            expect(defEli.get("default")).to.equal(true);
        });

        var server;

        beforeEach(function() {
            server = sinon.fakeServer.create();
        });

        afterEach(function () {
            server.restore();
        });

        it("Delete eligibility", function () {
            server.respondWith("DELETE", "/eligibility/1",
                [200, { "Content-Type": "application/json" },
                    JSON.stringify({})
                ]);

            var newEligibility = new EligibilityApp.Entities.Eligibility({objectId: "1"});
            eligibilityCollection.add(newEligibility);
            expect(eligibilityCollection.length).to.equal(3);

            eligibilityCollection.deleteItem(newEligibility);

            server.respond(); // Process all requests so far

            expect(eligibilityCollection.length).to.equal(2);
            var deletedEligibility = eligibilityCollection.get("1");
            expect(deletedEligibility).to.not.exist;
        });
    });
});