var expect = chai.expect;
describe('Eligibility Controller', function() {
    it('Controller not null', function() {
        expect(EligibilityApp.Eligibility.Controller).to.exist;
    });
    var server;

    beforeEach(function() {
        server = sinon.fakeServer.create();
    });

    afterEach(function () {
        server.restore();
    });

    it("Load Data", function () {
        server.respondWith("GET", "/eligibility/",
            [200, { "Content-Type": "application/json" },
                JSON.stringify({
                    "results": [
                        {
                            "createdAt": "2015-12-12T17:29:03.106Z",
                            "default": true,
                            "objectId": "ZYhVxiDASX"
                        }
                    ]
                })
            ]);

        server.respondWith("GET", "/Plan/",
            [200, { "Content-Type": "application/json" },
                JSON.stringify({
                    "results": [
                        {
                            "createdAt": "2015-12-12T17:29:03.106Z",
                            "type": "dental",
                            "name": "testPlan",
                            "objectId": "ZYhVxiDASX"
                        }
                    ]
                })
            ]);

        EligibilityApp.Eligibility.Controller.loadData();

        server.respond(); // Process requests

        expect(EligibilityApp.Eligibility.Controller.collection.length).to.equal(1);
        expect(EligibilityApp.Eligibility.Controller.getPlans().length).to.equal(1);
    });

    it("Add New eligibility", function () {
        server.respondWith("POST", "/eligibility/",
            [201, { "Content-Type": "application/json" },
                JSON.stringify({"createdAt":"2015-12-14T20:33:07.408Z","objectId":"OjYO3ckvPa"})
            ]);

        EligibilityApp.Eligibility.Controller.addNew();

        server.respond(); // Process requests

        expect(EligibilityApp.Eligibility.Controller.collection.length).to.equal(2);

        var newAdded = EligibilityApp.Eligibility.Controller.collection.get("OjYO3ckvPa");
        expect(newAdded).to.exist;

        expect(newAdded.get("order")).to.equal(1);
    });

    it("Delete eligibility", function () {
        server.respondWith("DELETE", "/eligibility/1",
            [200, { "Content-Type": "application/json" },
                JSON.stringify({})
            ]);

        var newAdded = EligibilityApp.Eligibility.Controller.collection.get("OjYO3ckvPa");
        EligibilityApp.Eligibility.Controller.remove(newAdded);

        server.respond(); // Process requests

        expect(EligibilityApp.Eligibility.Controller.collection.length).to.equal(1);
    });
});