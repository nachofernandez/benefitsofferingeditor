var expect = chai.expect;

describe('Plans Collection', function() {
    describe('Create new plans collection', function() {
        var plansCollection = new EligibilityApp.Entities.PlanCollection();

        it('Collection not null', function() {
            expect(plansCollection).to.exist;
        });

        it('Collection must be empty null', function() {
            expect(plansCollection.length).to.equal(0);
        });

        it('Add plans', function() {
            var medicalPlan1 = new EligibilityApp.Entities.Plan({type: EligibilityApp.plan_types.MEDICAL});
            plansCollection.add(medicalPlan1);

            var medicalPlan2 = new EligibilityApp.Entities.Plan({type: EligibilityApp.plan_types.MEDICAL});
            plansCollection.add(medicalPlan2);

            var dentalPlan1 = new EligibilityApp.Entities.Plan({type: EligibilityApp.plan_types.DENTAL});
            plansCollection.add(dentalPlan1);

            expect(plansCollection.length).to.equal(3);
        });

        it('Get plans by type', function() {
            var medicalPlans = plansCollection.getByPlan(EligibilityApp.plan_types.MEDICAL);
            expect(medicalPlans.length).to.equal(2);

            var dentalPlans = plansCollection.getByPlan(EligibilityApp.plan_types.DENTAL);
            expect(dentalPlans.length).to.equal(1);
            expect(dentalPlans[0].type).to.equal(EligibilityApp.plan_types.DENTAL);

            var lifePlans = plansCollection.getByPlan(EligibilityApp.plan_types.LIFE);
            expect(lifePlans).to.be.empty;
        });


        var server;

        beforeEach(function() {
            server = sinon.fakeServer.create();
        });

        afterEach(function () {
            server.restore();
        });

        it("Fetch Collection", function () {
            server.respondWith("GET", "/Plan/",
                [200, { "Content-Type": "application/json" },
                    JSON.stringify({
                        "results": [
                            {
                                "createdAt": "2015-12-12T17:29:03.106Z",
                                "type": "medical",
                                "name": "medical 123",
                                "objectId": "ZYhVxiDASX"
                            }
                        ]
                    })
                ]);

            plansCollection.fetch();

            server.respond(); // Process all requests so far

            expect(plansCollection.length).to.equal(1);
        });


    });
});

